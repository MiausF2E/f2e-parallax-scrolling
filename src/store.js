import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    routerTransition: 'fade'
  },
  mutations: {
    ChangeTransition (state, transition) {
      state.routerTransition = transition
    }
  },
  actions: {

  }
})
